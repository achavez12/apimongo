const Express = require("express");
const BodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;

const CONNECTION_URL = "mongodb+srv://viewbbva:categorizador23@webviewdb-2hslo.mongodb.net/test?retryWrites=true&w=majority";
const DATABASE_NAME = "viewBbvaDB"

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

var database, collection_usuarios, collection_transacciones;

app.listen(3052, () => {
  const client = new MongoClient(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true });
  client.connect(err => {
    database = client.db(DATABASE_NAME);
    collection_usuarios = database.collection("USUARIOS");
    collection_transacciones = database.collection("TRANSACCIONES");
    console.log('API Mongo is running...');
    //console.log("Connected to `" + DATABASE_NAME + "`!");
    // perform actions on the collection object
    //client.close();
    });
});

//root

app.get('/',function(req,res) {
  res.send('API Mongo');
})

// USUARIOS

app.post("/usuario", (request, response) => {
    collection_usuarios.insert(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result.result);
    });
});

app.get("/usuarios", (request, response) => {
    collection_usuarios.find({}).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

app.get("/usuarios/:id", (request, response) => {
    collection_usuarios.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

//LOGIN

app.post("/login", (request, response) => {
    collection_usuarios.findOne(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

// TRANSACCIONES

app.get("/transacciones", (request, response) => {
    collection_transacciones.find({}).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

app.get("/transacciones/:id", (request, response) => {
    collection_transacciones.findOne({ "_id": new ObjectId(request.params.id) }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
});

app.post("/transaccion", (request, response) => {
    collection_transacciones.insert(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result.result);
    });
});
